import os
import pickle
import subprocess
from time import time
from functools import partial
from multiprocessing import cpu_count, Pool
from PIL import Image

import numpy as np
import scipy.cluster.vq as vq
from numba import jit

from sklearn.cluster import MiniBatchKMeans
from sklearn.grid_search import GridSearchCV
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression

from utils.consts import IMAGES_PATH, DETECT_PATH, EXTRACT_PATH, \
    MODEL_PATH, CODEBOOK_PATH, TF_IDF_PATH

MAXSIZE = 1000 # used for resizing images
N_FEATURES = -1 # the number of sift features to retain
K_THRESH = 1 # stop value for KMeans
N_CLUSTERS = 20000 # the number of words in the codebook
N_THREADS = cpu_count()


def timeit(func):
    def wrapper(*args, **kwargs):
        start_time = time()
        result = func(*args, **kwargs)
        print("Finished in %d seconds" % (time() - start_time))
        return result
    return wrapper


def get_files(images):
    return np.hstack(map(lambda x: os.path.join(IMAGES_PATH, x + ".jpg"), images['sku']))


@jit(cache=True)
def extract_features(file_name, result_name):
    # create a pgm file, image is resized, if it is too big.
    # sift returns an error if more than 8000 features are found
    size = (MAXSIZE, MAXSIZE)

    image_name = file_name + ".pgm"
    hesaff_name = file_name + ".hesaff"
    st = os.stat(file_name)

    if st.st_size > 2000:
        img = Image.open(file_name).convert('L')
        img.thumbnail(size, Image.ANTIALIAS)
        img.save(image_name)

        detect_cmmd = DETECT_PATH + " -hesaff -i " + image_name + " -o " + hesaff_name + " -thres 500"
        extract_cmmd = EXTRACT_PATH + " -sift -i " + image_name + " -p1 " + hesaff_name + " -o1 " + result_name

        subprocess.call(detect_cmmd, shell=True)
        subprocess.call(extract_cmmd, shell=True)

        os.remove(image_name)
        return True

    return False


@jit(cache=True)
def get_sift(fname, limit=-1):
    features_fname = fname + ".sift"
    success = True

    if not os.path.exists(features_fname):
        success = extract_features(fname, features_fname)
        
    if success:
        descriptors = np.loadtxt(features_fname, skiprows=2)[:, -128:].astype('uint8')
    else:
        descriptors = [0]

    if limit == -1:
        limit = len(descriptors)

    return descriptors[:limit]


@timeit
def get_codebook(file_names, file_labels, r_codebook):
    if os.path.exists(CODEBOOK_PATH) and not r_codebook:
        codebook = np.load(CODEBOOK_PATH)
    else:
        with Pool(N_THREADS) as pool:
            features = pool.map(partial(get_sift, limit=N_FEATURES), file_names)
        
        kmeans = MiniBatchKMeans(N_CLUSTERS, tol=K_THRESH)
        kmeans.fit(np.vstack(list(filter(lambda x: len(x) > 1, features))))

        codebook = kmeans.cluster_centers_

        np.save(CODEBOOK_PATH, codebook)

    return codebook


@jit(cache=True)
def get_histogram(fname, codebook):
    features = get_sift(fname)

    if len(features) == 1:
        histogram = [0]
    else:
        n_bins = len(codebook) + 1

        code, dist = vq.vq(features, codebook)
        histogram, bin_edges = np.histogram(code, bins=range(n_bins), density=True)

    return histogram


@timeit
@jit(cache=True)
def write_histograms(image_files, image_labels, codebook, histograms_path, r_hist):
    if not os.path.exists(histograms_path) or r_hist:
        pool = Pool(N_THREADS)
        hists = pool.map(partial(get_histogram, codebook=codebook), image_files)
        pool.close()
        pool.join()

        size = len(hists)
        labels = np.zeros(size)
        histograms = np.zeros((size, len(codebook)))
        hist_ind, curr_ind = 0, 0
        
        for curr_ind in range(size):
            if len(hists[curr_ind]) != 1:
                labels[hist_ind] = image_labels[curr_ind]
                histograms[hist_ind] = hists[curr_ind]
                hist_ind += 1

        np.save(histograms_path, 
                np.hstack((
                    histograms[:hist_ind], 
                    np.vstack(labels[:hist_ind])
                ))
               )
        print("{num} histograms written".format(num=hist_ind))


@timeit
def train(codebook, histograms_path, r_model):
    if not os.path.exists(MODEL_PATH) or r_model:
        features = np.load(histograms_path)
        X = features[:, :-1]
        y = features[:, -1]

        # perform tf-idf transformation
        idf_vec = np.log(len(X) / np.sum(X > 0, axis=0))
        idf_vec[~np.isfinite(idf_vec)] = 0
        X = X * idf_vec

#        clf = GridSearchCV(LogisticRegression(penalty='l2', multi_class='multinomial'),
#                           param_grid={
#                               'C': (0.01, 0.1, 1, 10, 100),
#                               'solver': ('lbfgs', 'newton-cg')},
#                           n_jobs=N_THREADS,
#                           pre_dispatch=1)

        clf = LogisticRegression(penalty='l2', multi_class='multinomial', C=1000, solver='lbfgs', n_jobs=N_THREADS)
#        clf = SVC(kernel='linear', C=100)
        clf.fit(X.astype('float64'), y.astype('uint8'))

        with open(MODEL_PATH, 'wb') as f:
            pickle.dump(clf, f, protocol=pickle.HIGHEST_PROTOCOL)

        np.save(TF_IDF_PATH, idf_vec)


@timeit
def test(codebook, histograms_path):
    features = np.load(histograms_path)
    X = features[:, :-1]
    y = features[:, -1]

    # perform tf-idf transformation
    idf_vec = np.load(TF_IDF_PATH)
    X = X * idf_vec

    with open(MODEL_PATH, 'rb') as f:
        clf = pickle.load(f)

    predictions = clf.predict(X)

    return predictions, y
