from multiprocessing import Pool, cpu_count
import os
from PIL import Image
import pandas as pd
import subprocess

from consts import TRAINING_IMAGES_PATH, TESTING_IMAGES_PATH, DATASET_PATH


training_images = pd.read_csv(TRAINING_IMAGES_PATH)
testing_images = pd.read_csv(TESTING_IMAGES_PATH)

def f(row):
    size = (1000, 1000)
    file_name = os.path.join(DATASET_PATH, row[1]['sku'] + ".jpg")
    image_name = file_name + ".pgm"
    hesaff_name = file_name + ".hesaff"
    sift_name = file_name + ".sift"

    if not os.path.exists(sift_name):
        try:
            img = Image.open(file_name).convert('L')
            img.thumbnail(size, Image.ANTIALIAS)
            img.save(image_name)

            detect_cmmd = "./h_affine.ln -hesaff -i " + image_name + " -o " + hesaff_name + " -thres 0"
            extract_cmmd = "./compute_descriptors.ln -sift -i " + image_name + " -p1 " + hesaff_name + " -o1 " + sift_name

            subprocess.run(detect_cmmd, shell=True)
            subprocess.run(extract_cmmd, shell=True)

            os.remove(image_name)
        except:
            pass


with Pool(cpu_count()) as pool:
    #pool.map(f, testing_images.iterrows(), chunksize=100)
    pool.map(f, training_images.iterrows(), chunksize=100) 
