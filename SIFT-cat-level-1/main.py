import argparse
from time import time

import pandas as pd
import numpy as np

from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import accuracy_score, confusion_matrix

from utils import tools
from utils.consts import TRAINING_IMAGES_PATH, TESTING_IMAGES_PATH, \
    TRAINING_HISTOGRAMS_PATH, TESTING_HISTOGRAMS_PATH


def main(r_model, r_codebook, r_histogram) :
    start_time = time()
    le = LabelEncoder()

    print("---------------------")
    print("## loading training data")

    training_images = pd.read_csv(TRAINING_IMAGES_PATH)
    training_files = tools.get_files(training_images)
    training_labels = le.fit_transform(training_images['category_id']).astype("uint8")

    print("---------------------")
    print("## computing the visual words via k-means")

    codebook = tools.get_codebook(training_files, training_labels, r_codebook)

    print("---------------------")
    print("## compute the visual words histograms for each training image")

    tools.write_histograms(training_files, 
                           training_labels,
                           codebook, 
                           TRAINING_HISTOGRAMS_PATH,
                           r_codebook or r_histogram)

    print("---------------------")
    print("## train classifier")

    tools.train(codebook, TRAINING_HISTOGRAMS_PATH, r_model or r_codebook or r_histogram)

    print("---------------------")
    print("## loading testing data")

    testing_images = pd.read_csv(TESTING_IMAGES_PATH)
    testing_files = tools.get_files(testing_images)
    testing_labels = le.transform(testing_images['category_id']).astype("uint8")
    
    print("---------------------")
    print("## compute the visual words histograms for each testing image")
    
    tools.write_histograms(testing_files, 
                           testing_labels,
                           codebook, 
                           TESTING_HISTOGRAMS_PATH,
                           r_codebook or r_histogram)
 
    print("---------------------")
    print("## testing model")
    
    predicted_labels, true_labels = tools.test(codebook, TESTING_HISTOGRAMS_PATH)

    conf_mat = confusion_matrix(true_labels, predicted_labels)
    np.save("confusion_matrix.npy", conf_mat)

    print(accuracy_score(true_labels, predicted_labels))
    print("Total time: %d seconds" % (time() - start_time))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Rewrites files")
    parser.add_argument('-a', '--all', dest="r_all", default=False,
                        action="store_true", help="rewrite all files")
    parser.add_argument('-m', '--model', dest="r_model", default=False,
                        action="store_true", help="rewrite model file")
    parser.add_argument('-c', '--code', dest="r_codebook", default=False,
                        action="store_true", help="rewrite codebook file")
    parser.add_argument('-hi', '--histogram', dest="r_histogram", default=False,
                        action="store_true", help="rewrite historam file")
    args = parser.parse_args()

    main(args.r_all or args.r_model, 
        args.r_all or args.r_codebook,
        args.r_all or args.r_histogram)
