import os
import pickle
import subprocess

from time import time, sleep
from functools import partial
from multiprocessing import cpu_count, Pool
from PIL import Image, ImageFile
from math import ceil

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.cluster.vq as vq

from sklearn.cluster import MiniBatchKMeans
from sklearn.cross_validation import StratifiedKFold
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import *

from utils.consts import IMAGES_PATH, HISTOGRAMS_PATH, ADULT_CATEGORIES
from numba import jit

IMG_WIDTH, IMG_HEIGHT = 1000, 1000 # used for resizing images
N_FEATURES = 100 # the number of sift feautres to retain when clustering
K_THRESH = 1 # stop value for KMeans
N_CLUSTERS = 2000 # the number of words in the codebook
CHUNK_SIZE = 10000 # limits the number of files being processed
ImageFile.LOAD_TRUNCATED_IMAGES = True # deals with missing data in images
N_THREADS = cpu_count()


def timeit(func):
    def wrapper(*args, **kwargs):
        start_time = time()
        result = func(*args, **kwargs)
        print("Finished in %d seconds" % (time() - start_time))
        return result
    return wrapper


@jit(cache=True)
def get_img(sku, width=-1, height=-1):
    img_path = os.path.join(IMAGES_PATH, sku + ".jpg")

    if os.path.exists(img_path):
        st = os.stat(img_path)
        
        if st.st_size > 1000:
            img = Image.open(img_path).convert('L')
            if width != -1 and height != -1:
                img.thumbnail((width, height), Image.ANTIALIAS)

            return img


@jit(cache=True)
def extract_sift(sku):
    img_path = os.path.join(IMAGES_PATH, sku + ".jpg")
    sift_path = img_path + ".sift"

    if os.path.exists(img_path) and not os.path.exists(sift_path):
        pgm_path = img_path + ".pgm"
        hesaff_path = img_path + ".hesaff"
        st = os.stat(img_path)

        if st.st_size > 1000:
            img = Image.open(img_path).convert('L')
            img.thumbnail((IMG_WIDTH, IMG_HEIGHT), Image.ANTIALIAS)
            img.save(pgm_path)

            detect_cmmd = "./h_affine.ln -hesaff -i " + pgm_path + " -o " + hesaff_path + " -thres 0"
            extract_cmmd = "./compute_descriptors.ln -sift -i " + pgm_path + " -p1 " + hesaff_path + " -o1 " + sift_path

            subprocess.run(detect_cmmd, shell=True)
            subprocess.run(extract_cmmd, shell=True)

            os.remove(pgm_path)


@jit(cache=True)
def get_sift(sku, limit=10000):
    img_path = os.path.join(IMAGES_PATH, sku + ".jpg")
    sift_path = img_path + ".sift"

    if os.path.exists(sift_path):
        descriptors = np.genfromtxt(sift_path, skip_header=2, filling_values=0, invalid_raise=False)
        if not isinstance(descriptors[0], np.ndarray):
            descriptors = np.array([descriptors[-128:]], dtype="uint8")
        else:
            descriptors = descriptors[:min(limit, len(descriptors)), -128:].astype("uint8")
    else:
        descriptors = 0
    
    return descriptors


@timeit
def get_codebook(skus, r_code, code_path):
    if os.path.exists(code_path) and not r_code:
        codebook = np.load(code_path)
    else:
        pool = Pool(N_THREADS)
        f = partial(get_sift, limit=N_FEATURES)
        features = pool.map(f, skus, chunksize=100)
        pool.close()
        pool.join()
        
        features = np.vstack(filter(lambda x: isinstance(x, np.ndarray), features))
        print("Collected %d features" % len(features))
        kmeans = MiniBatchKMeans(N_CLUSTERS, batch_size=100)
        kmeans.fit(features)

        codebook = kmeans.cluster_centers_
        np.save(code_path, codebook)

    return codebook


@jit(cache=True)
def write_hist(sku, codebook, path, rewrite=False):
    hist_path = os.path.join(path, sku + ".hist.npy")
    features = get_sift(sku)
    n_bins = len(codebook) + 1

    if (not os.path.exists(hist_path) or rewrite) and isinstance(features, np.ndarray):
        code, dist = vq.vq(features, codebook)
        histogram, bin_edges = np.histogram(code, bins=range(n_bins), density=True)
        np.save(hist_path, histogram)


@timeit
@jit(cache=True)
def get_hists(skus, codebook, path=HISTOGRAMS_PATH, rewrite=False):
    pool = Pool(cpu_count())
    chunk_size = 10000
    size = len(skus)
    num_chunks = ceil(size / chunk_size)

    print("--- Expecting to work on {num} chunks ---".format(num=num_chunks))
    f = partial(write_hist, codebook=codebook, path=path, rewrite=rewrite)

    for ind in range(num_chunks):
        start, end = ind * chunk_size, min(size, (ind + 1) * chunk_size)
        total = end - start + 1
        print("Working on files {start} through {end}".format(start=start, end=end))

        result = pool.map_async(f, skus[start:end], chunksize=50)

        while (True):
            if result.ready():
                break
            left = result._number_left
            percentage = (total - left) * 100 / total
            print("Completed {0}%".format(percentage))
            sleep(60)

        print("--- Completed chunk #{num} ---".format(num=ind))
        
    pool.close()
    pool.join()


@jit(cache=True)
def load_hists(skus, labels, path=HISTOGRAMS_PATH):
    hists = np.zeros((len(skus), N_CLUSTERS))
    lbls = np.zeros(len(labels), dtype='uint8')
    ind = 0

    for i, sku in enumerate(skus):
        hist_path = os.path.join(path, sku + ".hist.npy")
        if os.path.exists(hist_path):
            hist = np.load(hist_path)
            if len(hist) == N_CLUSTERS:
                hists[ind] = np.load(hist_path)
                lbls[ind] = labels.iloc[i]
                ind += 1

    return hists[:ind], lbls[:ind]


def get_histogram_data(df, path=HISTOGRAMS_PATH):
    le = LabelEncoder()
    df['label'] = le.fit_transform(df['category_id'])
    
    data, labels = load_hists(df['sku'], df['label'], path)

    skf = StratifiedKFold(labels, random_state=1334)
    for train_index, test_index in skf:
        train_data, test_data = data[train_index], data[test_index]
        train_label, test_label = labels[train_index], labels[test_index]
        break

    return train_data, train_label, test_data, test_label, le


def plot_roc(true_labels, pred_probs, true_label):
    fpr, tpr, _ = roc_curve(true_labels, pred_probs, pos_label=true_label)
    roc_auc = auc(fpr, tpr)

    plt.figure()
    plt.plot(fpr, tpr)
    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC curve (area = %0.2f)' % roc_auc)
    plt.show()


def plot_prec(true_labels, pred_probs, true_label):
    precision, recall, _ = precision_recall_curve(true_labels, pred_probs, pos_label=true_label)
    # average_precision = average_precision_score(true_labels, pred_probs, average='weighted')

    plt.figure()
    plt.plot(recall, precision)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.title('Precision-Recall curve')
    plt.show()


def plot_confusion_matrix(cm, label=0, normalied=True, title='Confusion matrix', cmap=plt.cm.Blues):
    print("Precision: %.2f" % (cm[label, label] / np.sum(cm, axis=0)[label]))
    print("Recall: %.2f" % (cm[label, label] / np.sum(cm, axis=1)[label]))
    
    if normalied:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        title = 'Normalized confusion matrix'

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


@timeit
def test_classifier(clf, train_data, train_label, test_data, test_label, true_label=0, plot=True):
    clf.fit(train_data, train_label)
    
    if hasattr(clf, 'predict_proba') and callable(getattr(clf, 'predict_proba')):
        clf_prob = clf.predict_proba(test_data)
        clf_label = np.argmax(clf_prob, axis=1)
        
        if plot:
            plot_roc(test_label, clf_prob[:, true_label], true_label)
            plot_prec(test_label, clf_prob[:, true_label], true_label)
    else:
        clf_prob = None
        clf_label = clf.predict(test_data)

    clf_conf_mat = confusion_matrix(test_label, clf_label)
    
    if plot:
        plot_confusion_matrix(clf_conf_mat, true_label)

    return clf, clf_prob, clf_conf_mat