from utils.tools import *
from utils.consts import *

import matplotlib.pyplot as plt
import xgboost as xgb
import importlib

from sklearn.decomposition import PCA, RandomizedPCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.manifold import TSNE
from sklearn import preprocessing

from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, VotingClassifier
from sklearn.neighbors import KNeighborsClassifier, RadiusNeighborsClassifier

from sklearn.metrics import *
from sklearn.cross_validation import StratifiedKFold, KFold

from hyperopt import fmin, tpe, hp, STATUS_OK, Trials