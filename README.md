# Image classification
Here is all you need to perform SIFT-based image classification except for image files. All .csv files have an image url column if you need to download images.

## Level 1 category classification
All files are stored in SIFT-cat-level-1 folder. Run main.py script to train and test a classifier on the prepared data. You can use -h option to 
see some other options which will be helpful to rewrite old files when you split the data differently. Unfortunately, you have to split the data manually.

## Adult image classification
You can just open SIFT.ipynb file and step through the code. There is another approach to tackle the classification problem in PCA.ipynb, but 
it's only a template.
